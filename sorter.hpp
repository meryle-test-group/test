#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sys/stat.h>

enum sort_order {
    ASC,
    DESC
} typedef sortOrder;


void usage() {
    std::cout << "Usage: sorter -f <file> -o <sort_order ASC/DESC>\n" << std::endl;
}

void read_file(const std::string &filename, std::vector<std::string> &input_lines) {
    // ensure file exists
    struct stat buffer;
    if (stat(filename.c_str(), &buffer) != 0) {
        throw std::runtime_error("File does not exist");
    }

    std::ifstream file_stream(filename);
    std::string line;
    while (std::getline(file_stream, line)) {
        input_lines.push_back(line);
    }
}

void sort_lines(std::vector<std::string> &input_lines, const sortOrder sort_order) {
    if (input_lines.size() == 0) {
        throw std::runtime_error("Empty input vector");
    }
    switch(sort_order) {
        case ASC:
            std::sort(input_lines.begin(), input_lines.end());
            break;
        case DESC:
            std::sort(input_lines.begin(), input_lines.end(), std::greater<std::string>());
            break;
        default:
            throw std::runtime_error("Invalid sort order");
    }
}

void read_input_args(int argc, char* argv[], std::string &myfile, sortOrder &sort_order) {
    if (argc != 5) {
        usage();
        throw std::invalid_argument("Invalid number of arguments");
    }

    for (int i = 1; i < argc; i++) {
        if (std::string(argv[i]) == "-f") {
            myfile = argv[i + 1];
        }
        if (std::string(argv[i]) == "-o") {
            if (std::string(argv[i + 1]) == "ASC") {
                sort_order = ASC;
            } 
            else if (std::string(argv[i + 1]) == "DESC") {
                sort_order = DESC;
            } 
            else {
                std::cout << "Invalid sort order. Must be ASC or DESC\n" << std::endl;
                throw std::runtime_error("Invalid sort order");
            }
        }
    }
}
