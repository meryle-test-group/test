#include "sorter.hpp"
#include <iostream>
#include <sys/stat.h>


// main is where the action is
int main(int argc, char* argv[]) {

    std::string myfile;
    sortOrder sort_order;

    // parse and validate input args
    if (argc != 5) {
        usage();
        return 1;
    }

    try {
        read_input_args(argc, argv, myfile, sort_order);
    } catch (std::runtime_error& e) {
        std::cout << e.what() << std::endl;
        std::cout << "Exiting...\n" << std::endl;
        return 1;
    }

    // ensure file exists
    struct stat buffer;
    if (stat(myfile.c_str(), &buffer) != 0) {
        std::cout << "File does not exist\n" << std::endl;
        return 1;
    }

    // open the file and read the contents into a vector
    std::vector<std::string> input_lines;
    read_file(myfile, input_lines);

    // error if there are 0 lines in the file. If there is only one line, 
    // I guess sorting is pretty straightforward (lol) but it isn't technically an error.
    if (input_lines.size() == 0) {
        std::cout << "File is empty\n" << std::endl;
        return 1;
    }

    // sort the vector according to the specified sort order from the args
    try {
        sort_lines(input_lines, sort_order);
    }
    catch (std::exception& e) {
        std::cout << e.what() << std::endl;
        std::cout << "Exiting...\n" << std::endl;
        return 1;
    }

    // print the sorted vector to the console
    for (auto line : input_lines) {
        std::cout << line << std::endl;
    }

    return 0;
}
