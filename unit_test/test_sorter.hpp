#include <cxxtest/TestSuite.h>
#include "../sorter.hpp"


class SorterTest : public CxxTest::TestSuite {
public:
    void testReadFile() {
        std::vector<std::string> lines;
        read_file("../names_list.txt", lines);

        TS_ASSERT_EQUALS(lines.size(), 12);
        TS_ASSERT_EQUALS(lines[0], "This is my name");
    }

    void testReadFileFail() {
        std::vector<std::string> lines;
        TS_ASSERT_THROWS(read_file("non_existent_file.txt", lines), std::runtime_error);
    }

    void testReadInputArgs() {
        std::string myfile;
        sortOrder order;

        char* argv[] = {strdup("program"), strdup("-f"), strdup("../names_list.txt"), strdup("-o"), strdup("ASC")};
        read_input_args(5, argv, myfile, order);

        TS_ASSERT_EQUALS(myfile, "../names_list.txt");
        TS_ASSERT_EQUALS(order, ASC);

        for(int i = 0; i < 5; i++) {
            free(argv[i]);
        }
    }

    void testReadInputArgsFail() {
        std::string myfile;
        sortOrder order;

        char* argv[] = {strdup("program"), strdup("-f"), strdup("-t"), strdup("ASC")};
        TS_ASSERT_THROWS(read_input_args(4, argv, myfile, order), std::invalid_argument);

        for(int i = 0; i < 4; i++) {
            free(argv[i]);
        }
    }

    void testSortLinesAsc() {
        std::vector<std::string> lines = {"Alfred", "Batman", "Catwoman"};
        sort_lines(lines, ASC);

        TS_ASSERT_EQUALS(lines[0], "Alfred");
        TS_ASSERT_EQUALS(lines[1], "Batman");
        TS_ASSERT_EQUALS(lines[2], "Catwoman");
    }

    void testSortLinesDesc() {
        std::vector<std::string> lines = {"Alfred", "Batman", "Catwoman"};
        sort_lines(lines, DESC);

        TS_ASSERT_EQUALS(lines[0], "Catwoman");
        TS_ASSERT_EQUALS(lines[1], "Batman");
        TS_ASSERT_EQUALS(lines[2], "Alfred");
    }

    void testSortLinesFail() {
        std::vector<std::string> lines;
        TS_ASSERT_THROWS(sort_lines(lines, ASC), std::runtime_error);
    }
};
