# Amgis Test, Meryle Swartz: Sorter

## Setting up the environment

First, install the necessary dependencies:

Ubuntu/Debian:
```bash
sudo apt-get install -y cmake cxxtest
```
Fedora/Redhat:
```bash
sudo dnf update -y
sudo dnf install cmake -y
sudo dnf install cxxtest -y
```

Then:
```bash
git clone https://gitlab.com/meryle1/test.git
```

## Building locally

```bash
cd test
mkdir build
cd build
cmake ..
make
```

## Running the program and/or the unit tests

After a successful build, you will find two executable files in the `test/build` directory:

- `test_sorter`
- `sorter`

Running `test_sorter` will execute the unit tests. This binary does not take any arguments.
Executing `sorter` will run the application. It requires the following arguments:
- `-f <path_to_file>`
- `-o <ASC/DESC>`

Only one of ASC or DESC can be specified at a time. The path to the file can be a relative or an absolute path.

The included test list of names, `names_list.txt`, is utilized by `test_sorter`. 
It can also be passed in to the `sorter` as follows:

```bash
sorter -f ../names_list.txt -o ASC
```

Replace `ASC` with `DESC` if you want to sort in descending order.
